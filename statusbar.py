#!/usr/bin/env python3
#
# Copyright 2019 Morten Jakobsen. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

# This file does not follow PEP standards... I KNOW!!! It was not written
# with that in mind. I just needed something quick and dirty. So there it
# is. This script is due for an update anyway, so that might come.

from requests import get
from os import environ
from sys import exit

API_KEY = environ['TWITCHAPI']
OAUTH_KEY = environ['TWITCHOAUTHSUBS']

streamObj = {
    'viewers': ['👀', None],
    'followers': ['🧟', None],
    'subscribers': ['🦸', None]
}

# Grab the selected username of the current stream
with open("/home/mj/.config/nginx/chosen", 'r') as f:
    username = f.read().strip()

# Grab userid from username
r = get("https://api.twitch.tv/helix/users?login={}".format(username), headers={"Client-ID": API_KEY}).json()
try:
    userid = r['data'][0]['id']
except IndexError as e:
    print("Unable to fetch userID")
    exit(1)

# Figure out if live and grab viewer count
r = get("https://api.twitch.tv/helix/streams?user_login={}".format(username), headers={"Client-ID": API_KEY}).json()
try:
    if len(r['data']) > 0:
        streamObj['viewers'][1] = r['data'][0]['viewer_count']
except Exception as e:
    pass

# Grab number of followers
r = get("https://api.twitch.tv/helix/users/follows?to_id={}".format(userid), headers={"Client-ID": API_KEY}).json()
streamObj['followers'][1] = r['total']

# Grab subscribers
if username == "jakeobsen":
    r = get("https://api.twitch.tv/helix/subscriptions?broadcaster_id={}".format(userid), headers={'Authorization': "Bearer {}".format(OAUTH_KEY)}).json()
    streamObj['subscribers'][1] = len(r['data'])

# Build statusline
statusbar = []
for sOut in streamObj.items():
    if sOut[1][0] is not None and sOut[1][1] is not None:
        statusbar.append("{} {}".format(sOut[1][0], sOut[1][1]))

# Output statusline
print(" ".join(statusbar))
print(" ".join(statusbar))
