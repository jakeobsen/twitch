#!/usr/bin/env python3
#
# Copyright 2019 Morten Jakobsen. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

# You will need an environment key named TWITCHAPI too
# You can probably add that to your .profile like such:
# export TWITCHAPI="twitch client-id token here"

from requests import get
from os import environ
from argparse import ArgumentParser

API_KEY = environ['TWITCHAPI']

def c(length, text):
    """
    Generate column

    :length: Length of column
    :text: Text to be formatted
    :return: Columnised text
    :rtype: string
    """
    whitespace = length - len(text)
    return text+whitespace*" "

class Twitch:
    """
    This twitch class will take one username when initialized and print a
    list of the users followers, who are currently streaming.
    The usernames will be listed with number of viewers and the current
    game title.
    """

    def __init__(self, username):
        """
        Class initiator, sets required variables and executes

        :username: Username to lookup followers of
        :return: None
        """
        self.streams = []
        self.me = {
                "userID": self.getUserID(username),
                "username": username,
                "follows": []
                }

        self.getFollows()
        self.getFollowersInfo()
        self.printPrettyList()

    def apiRequest(self, apiCall):
        """
        Performs API call

        :return: Returns results from API request
        :rtype: dict
        """
        r = get("https://api.twitch.tv/helix/{}".format(apiCall),
                headers={"Client-ID": API_KEY}).json()
        return r

    def getUserID(self, username):
        """
        Get userID from username

        :username: Username to lookup
        :return: Returns userID from given username
        :rtype: string
        """
        data = self.apiRequest("users?login={}".format(username))
        try:
            userID = data['data'][0]['id']
        except IndexError:
            userID = ""

        return userID

    def getFollows(self):
        """
        Get followers

        :return: None
        """
        
        cursor = ""
        while True:
            data = self.apiRequest("users/follows?first=100&from_id={}{}".
                    format(self.me['userID'], cursor))

            for r in data['data']:
                self.me['follows'].append(r['to_id'])

            if len(data['data']) > 0:
                cursor = "&after={}".format(data['pagination']['cursor'])
            else:
                break

    def getFollowersInfo(self):
        """
        Build stream object from follower info

        :return: None
        """

        index = 0
        total = len(self.me['follows'])
        while index < total:
            old_index = index
            index += 100
            id_list = self.me['follows'][old_index:index]

            # Get info for all followers
            twitch_user_id = "?user_id={}".format(
                        "&user_id=".join(id_list))

            data = self.apiRequest("streams{}".format(twitch_user_id))
            try:
                for u in data['data']:
                    self.streams.append({
                        "userid": u['user_id'],
                        "username": str(u['user_name']),
                        "title": str(u['title']),
                        "viewers": u['viewer_count'],
                        "gameid": u['game_id'],
                        "type": u['type']
                    })
            except IndexError:
                pass
            except KeyError:
                pass

    def printPrettyList(self):
        """
        Prints follower list in a pretty format ordered by viewers DESC

        :return: None
        """
        # Todo - look into this https://docs.python.org/3/library/pprint.html
        for s in sorted(self.streams, key=lambda d: d['viewers'], reverse=True):
            c1 = c(26, s['username'])
            c2 = c(6, str(s['viewers']))
            c3 = s['title'].strip()
            print(c1,c2,c3)

if __name__ == "__main__":
    # Description
    parser = ArgumentParser(description="Lookup current live users for username")
    # Username
    parser.add_argument('-u', help="Username to lookup", default="")
    # Parse input
    args = parser.parse_args()

    if args.u:
        t = Twitch(args.u)
